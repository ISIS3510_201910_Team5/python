import unittest
import index


class TestHandlerCase(unittest.TestCase):

    def test_response(self):
        print("testing response.")
        str = '{"url": "https://dbdermatologiabarcelona.com/wp-content/uploads/2012/11/Lunares-small-300x199.jpg"}'
        event = {"body": str}
        result = index.handler(event, None)
        print(result)
        self.assertEqual(result['statusCode'], 200)
        self.assertEqual(result['headers']['Content-Type'], 'application/json')
        self.assertIn('159', result['body'])


if __name__ == '__main__':
    unittest.main()
