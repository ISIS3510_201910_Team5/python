import json
import datetime
import numpy as np
import requests
import imageio as im


def handler(event, context):
    print("Received event: ")
    print(type(event))
    print(""+json.dumps(event, indent=2))
    print(event["body"])
    ulrJSON = event["body"]
    urlLoaded = json.loads(ulrJSON)
    print(urlLoaded)
    print(urlLoaded["url"])
    url = urlLoaded["url"]
    r=requests.get(url,allow_redirects=True)
    with open("/tmp/img.jpg","wb") as f:
        f.write(r.content)
    with open("/tmp/img.jpg", 'rb') as data:
        img = im.imread(data)
        avg = np.mean(img)
    data = {
        "output": avg,
        "timestamp": datetime.datetime.utcnow().isoformat()
    }
    return {"statusCode": 200,
            "body": json.dumps(data),
            "headers": {"Content-Type": "application/json"}};
